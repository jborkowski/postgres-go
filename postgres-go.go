package main

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strings"

	"context"

	_ "github.com/lib/pq"
)

const (
	CORE_N = 2
	HDD_N  = 1

	N_CONNS     = ((CORE_N * 2) + HDD_N)
	DB_USER     = "dbuser"
	DB_PASSWORD = "1qaz2wsx"
	DB_NAME     = "bellevue"
	DB_HOST     = "192.168.41.113"

	EMPLOYEEID    = "r3.resource_id = "
	EMPLOYEELOGIN = "tu.user_login = "
	SUPERIORID    = "r3.superior_id = "

	QUERY = `SELECT r3.resource_id AS employee_id,
	    tu.user_login AS employee_login,
	    reorder_name(r3.name) AS employee_full_name,
	    r3.email AS employee_email,
	    r3.role_code AS employee_grade_code,
	        CASE
	            WHEN r3.role::text ~~ '%(%'::text THEN "substring"(r3.role::text, 0, "position"(r3.role::text, '('::text))::character varying
	            ELSE r3.role
	        END AS employee_grade,
	    r3.superior_id,
	    reorder_name(r2.name) AS superior_name,
	    r2.email AS superior_email,
	    aai.anal_desc AS location,
	    r3.department,
	    r3.department_code,
	    r3.is_active
	   FROM ( SELECT r.resource_id,
	            r.name,
	            r.superior_id,
	            r.email,
	            r.is_active,
	            r.location,
	            d.department,
	            d.department_code,
	            d.role,
	            d.role_code
	           FROM v1.res_resources r
	             LEFT JOIN ( SELECT rr_1.resource_id,
	                    rr_1.name AS department,
	                    rr_1.code AS department_code,
	                    rc.role,
	                     rc.role_code
	                     FROM (v1.res_resourcedet rr
	                     LEFT JOIN v1.res_departments rd ON rr.department_id = rd.department_id) rr_1(resource_id, department_id, category_id, department_id_1, name, code, manager_id)
	                     LEFT JOIN v1.res_categories rc ON rr_1.category_id = rc.category_id) d ON d.resource_id = r.resource_id) r3
	    LEFT JOIN v1.res_resources r2 ON r3.superior_id = r2.resource_id
	    JOIN v1.user_resource ur ON ur.usrc_res_id = r3.resource_id
	    LEFT JOIN v1.tuser tu ON tu.user_id = ur.usrc_user_id
	    LEFT JOIN v1.anal_all_items aai ON aai.anal_code::text = r3.location::text WHERE `
)

var (
	ctx    context.Context
	dbinfo = fmt.Sprintf("host=%s port=5432 user=%s password=%s dbname=%s sslmode=disable", DB_HOST, DB_USER, DB_PASSWORD, DB_NAME)

	db = createDBConnection()
)

type Response struct {
	Employee_id         int    `json:"employee_id"`
	Employee_login      string `json:"employee_login"`
	Employee_full_name  string `json:"employee_full_name"`
	Employee_email      string `json:"employee_email"`
	Employee_grade_code string `json:"employee_grade_code"`
	Employee_grade      string `json:"employee_grade"`
	Superior_id         int    `json:"superior_id"`
	Superior_name       string `json:"superior_name"`
	Superior_email      string `json:"superior_email"`
	Location            string `json:"location"`
	Department          string `json:"department"`
	Department_code     string `json:"department_code"`
	Is_active           string `json:"is_active"`
}

func main() {
	http.HandleFunc("/employee", employeeHandler)
	log.Println(http.ListenAndServe(":8080", nil))
}

func employeeHandler(w http.ResponseWriter, r *http.Request) {
	var buffer bytes.Buffer

	params, err := url.ParseQuery(r.URL.RawQuery)
	if err != nil {
		fmt.Fprintf(w, "Can't parse query: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	employeeID, okID := params["employee_id"]
	employeeLogin, okLogin := params["employee_login"]
	superiorID, okSID := params["superior_id"]
	if (!okID) && (!okLogin) && (!okSID) {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, "You must provide EmployeeId or SuperiorId or EmployeeLogin")
		return
	}

	buffer.WriteString(QUERY)

	if okID {
		buffer.WriteString(EMPLOYEEID)
		buffer.WriteString(employeeID[0])
	}

	if okLogin {
		if okID {
			buffer.WriteString(" , ")
		}
		buffer.WriteString(EMPLOYEELOGIN)
		buffer.WriteString("'" + strings.ToUpper(employeeLogin[0]) + "'")
	}

	if okSID {
		if okLogin {
			buffer.WriteString(" , ")
		}
		buffer.WriteString(SUPERIORID)
		buffer.WriteString(superiorID[0])
	}

	json, err := getJSON(buffer.String())
	// if err != nil {
	// 	w.WriteHeader(http.StatusBadRequest)
	// }
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(json))
}

type Queryer interface {
	Query(query string, args ...interface{}) (*sql.Rows, error)
}

func getJSON(sqlString string) (string, error) {
	// context.Background()
	// newCtx, done := context.WithTimeout(ctx, time.Second*2)
	rows, err := db.Query(sqlString)
	if err == sql.ErrNoRows {
		return "", err
	}
	if err != nil {
		return "", err
	}
	defer rows.Close()
	columns, err := rows.Columns()
	if err != nil {
		return "", err
	}
	count := len(columns)
	var tableData []map[string]interface{}
	values := make([]interface{}, count)
	valuePtrs := make([]interface{}, count)
	for rows.Next() {
		for i := 0; i < count; i++ {
			valuePtrs[i] = &values[i]
		}
		rows.Scan(valuePtrs...)
		entry := make(map[string]interface{})
		for i, col := range columns {
			var v interface{}
			val := values[i]
			b, ok := val.([]byte)
			if ok {
				v = string(b)
			} else {
				v = val
			}
			entry[col] = v
		}
		tableData = append(tableData, entry)
	}
	jsonData, err := json.Marshal(tableData)
	if err != nil {
		return "", err
	}
	fmt.Println(string(jsonData))
	return string(jsonData), nil
}

// func (db *DB) queryWithContext(ctx context.Context) (*sql.Rows, error) {
//     receiveChan := make(chan *sql.Rows)
//     go func() {
//         receiveChan <- &MyFunctionReturningSomeTypeWithoutContext()
//     }
//     responseChan := make(chan *someType)
//     go func() {
//         select {
//         case <- ctx.Done():
//             responseChan <- nil
//             go func() {
//                 //Inaczej tamta gorutyna sie zablokuje na pisaniu do kanalu i bedzie leak gorutyny
//                 <- receiveChan
//             }
//         case myResponse := <- receiveChan:
//             responseChan <- myResponse
//         }
//     }

//     res := <- responseChan
//     if res == nil {
//         return nil, errors.Errorf("Context cancelled.")
//     }
//     return res, nil
// }

func createDBConnection() *sql.DB {
	//ctx := context.Background()
	//ctx, done := context.WithTimeout(ctx, time.Second*2)

	db, err := sql.Open("postgres", dbinfo)
	checkErr(err)
	checkErr(db.Ping())
	db.SetMaxIdleConns(N_CONNS)
	return db
}

func checkErr(err error) {
	if err != nil {
		fmt.Printf("Unexpected behaviour occurred: %s \n", err)
		panic(err)
	}
}
